export const mergeArray = array => {
  let output = [];
  array.forEach(function (item, i) {
    if (output.length > 0) {
      output.map(function (v, i) {
        if (v.tanggal_absen == item.tanggal_absen) {
          output.map(roeOut => {
            array.map((row, i) => {
              roeOut.items.push(row);
            });
          });
        } else {
          output.push({
            tanggal_absen: item.tanggal_absen,
            items: [],
          });
        }
      });
    } else {
      output.push({
        tanggal_absen: item.tanggal_absen,
        items: [],
      });
    }
  });
  return output;
};

export const orderData = (arr, type = 'desc') => {
  if (type === 'desc') {
    return arr.sort((a, b) => b.type - a.type);
  }
  return arr.sort((a, b) => a.type - b.type);
};
