import Geolocation from '@react-native-community/geolocation';
import MapboxGL from '@react-native-mapbox-gl/maps';
import axios from 'axios';
import {getDistance} from 'geolib';
import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {connect, useDispatch, useSelector} from 'react-redux';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import Button from '../../Component/Atoms/Button';
import LocationSearch from '../../Component/Molecules/LocationSearch';
import {MAPBOX_KEY} from '../../Config/constant';
import allActions from '../../Config/Redux/Actions';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoieXVkaWNhbmRyYTEiLCJhIjoiY2tuemd6dXhoMDR1ZDJ2cGMzbGk0dHpoaSJ9.Y5TZzkmHQd4Q2hWpDllpsQ',
);
const options = {
  enableHighAccuracy: true,
  timeout: 5000,
};
const LocationScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {location} = useSelector(state => state);
  const {locationData} = location;
  const [latitude, setLatitude] = useState(0);
  const [longitude, setLongitude] = useState(0);
  const [distance, setDistance] = useState(0);
  const [position, setPosition] = useState({});
  const [query, setQuery] = useState('');
  const [placeName, setPlaceName] = useState('');

  const {
    addPointLocation,
    searchPlace,
    clearSearchPlace,
  } = allActions.locationAction;

  useEffect(() => {
    getCurrentPosition();
  }, []);

  const getCurrentPosition = () => {
    Geolocation.getCurrentPosition(onSuccess, onError, options);
  };

  const onSuccess = ({coords}) => {
    setLatitude(coords.latitude);
    setLongitude(coords.longitude);
    console.log(coords);
    getAdressOnDrag(coords);
  };

  const onError = error => {
    console.log(error);
  };

  const onPointDrag = data => {
    let distance = getDistance(position, {
      latitude: data.geometry.coordinates[1],
      longitude: data.geometry.coordinates[0],
    });

    setLatitude(data.geometry.coordinates[1]);
    setLongitude(data.geometry.coordinates[0]);
    setDistance(distance / 1000);

    const coordinate = [
      data.geometry.coordinates[0],
      data.geometry.coordinates[1],
    ];
    getAdressOnDrag(coordinate);
  };

  const onChangeText = e => {
    setQuery(e.val);
    dispatch(searchPlace(e.val, null));
  };

  const handlePressItem = ({item, ref}) => {
    setLatitude(item.latitude);
    setLongitude(item.longitude);
    setPlaceName(item.place_name);
    setQuery('');
    ref.close();
    dispatch(clearSearchPlace());
  };

  const getAdressOnDrag = coordinate => {
    axios
      .get(
        `https://api.mapbox.com/geocoding/v5/mapbox.places/${coordinate}.json?types=poi&access_token=${MAPBOX_KEY}`,
      )
      .then(results => {
        setPlaceName(results.data.features[0].place_name);
      });
  };

  const handleSubmit = () => {
    const data = {nama_lokasi: placeName, latitude, longitude};
    dispatch(addPointLocation(data, navigation));
  };

  return (
    <ScrollView style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={{flex: 1, alignItems: 'center'}}>
        <View style={{alignItems: 'center'}}>
          <Text style={{fontSize: hp(3), fontWeight: 'bold', marginTop: hp(2), marginBottom: hp(4)}}>
            Lokasi Absen
          </Text>
          {/* <Text style={styles.headerContent}>
            Silahkan pilih lokasi anda untuk dijadikan titik absen harian anda.
          </Text> */}
        </View>
        <LocationSearch
          onChangeText={e => onChangeText(e)}
          value={query}
          data={locationData}
          onPress={e => handlePressItem(e)}
          label={placeName}
        />
        <View style={styles.mapContainer}>
          <View style={styles.map}>
            <TouchableOpacity
              onPress={() => getCurrentPosition()}
              underlayColor="#fff"
              style={styles.mapFocus}>
              <MaterialIcons name="filter-center-focus" size={hp(5)} />
            </TouchableOpacity>
            <MapboxGL.MapView
              style={{flex: 1, borderRadius: wp(5)}}
              zoomEnabled
              compassEnabled
              styleURL={MapboxGL.StyleURL.Street}
              radi
              localizeLabels>
              <MapboxGL.Camera
                zoomLevel={15}
                animationMode={'moveTo'}
                // animationDuration={1100}
                centerCoordinate={[longitude, latitude]}
                // followUserLocation={true}
                followUserMode={MapboxGL.UserTrackingModes.Follow}
                focusable

                // centerCoordinate={coordinates}
              />

              <MapboxGL.PointAnnotation
                key="key1"
                id="id1"
                title="Test"
                draggable
                onDragEnd={e => onPointDrag(e)}
                coordinate={[longitude, latitude]}
              />
              <MapboxGL.CircleLayer id="a" layerIndex={50} circleR />
            </MapboxGL.MapView>
          </View>
        </View>

        <View style={styles.footer}>
          <Button label="KONFIRMASI" onPress={() => handleSubmit()} />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  mapFocus: {
    position: 'absolute',
    bottom: hp(2),
    right: wp(3),
    zIndex: 9999,
    backgroundColor: '#fff',
    borderRadius: hp(1),
  },
  headerContent: {
    fontSize: hp(2),
    marginTop: hp(1),
    marginBottom: hp(2.6),
    textAlign: 'center',
  },
  footer: {
    flex: 1,
    width: wp(100),
    paddingHorizontal: wp(2.6),
  },
  mapContainer: {
    width: '100%',
    height: hp(68),
    zIndex: -1,
    paddingHorizontal: wp(3),
    borderRadius: wp(5),
  },
  map: {
    flex: 1,
    width: '100%',
    height: hp(60),
    backgroundColor: '#fff',
    borderRadius: wp(5),
  },
});

export default LocationScreen;
