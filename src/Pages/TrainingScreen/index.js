import AsyncStorage from '@react-native-async-storage/async-storage';
import {Component} from 'react';
import {DeviceEventEmitter, NativeModules} from 'react-native';
import {SET_INITIAL_ROUTE_NAME} from '../../Config/constant';
import {store} from '../../Config/Redux/store';

const {RecognitionModule} = NativeModules;
export default class TrainingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: false,
    };
    DeviceEventEmitter.addListener('onSuccess', this.onSuccess);
  }

  componentDidMount() {
    if (!this.state.status) {
      this.onShow();
    }
  }

  onShow = () => {
    return RecognitionModule.navigateToTrainingModel();
  };

  onSuccess = async e => {
    this.setState({status: e.status});
    let {isWfh} = store.getState().auth.user;
    if (isWfh == 1) {
      this.setState({status: true});
      await AsyncStorage.setItem('initialRouteName', 'LocationScreen');
      store.dispatch({type: SET_INITIAL_ROUTE_NAME, payload: 'LocationScreen'});
    } else {
      await AsyncStorage.setItem('initialRouteName', 'HomeScreen');
      store.dispatch({type: SET_INITIAL_ROUTE_NAME, payload: 'HomeScreen'});
    }
  };

  render() {
    return null;
  }
}
