import React, {useEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  ImageBackground,
} from 'react-native';
import {Snackbar} from 'react-native-paper';
import {useDispatch, useSelector} from 'react-redux';
import {CardBox} from '../../Component/Atoms/CardBox';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import allActions from '../../Config/Redux/Actions';
// import HistoryCard from '../../Component/Molecules/HistoryCard';
import Ionicons from 'react-native-vector-icons/Ionicons';

const HomeScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {setVisible, checkAttendance, getHistory} = allActions.attendanceAction;
  const {auth, attendance} = useSelector(state => state);
  const {visible, message, histories} = attendance;
  const {name, jabatan} = auth.user || {};

  useEffect(() => {
    dispatch(getHistory(null, false));
  }, []);

  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <StatusBar backgroundColor="#3742fa" translucent />
        <Snackbar visible={visible}>{message}</Snackbar>
        <View style={styles.cardHeader}>
          <View style={styles.cardHeaderBody}>
            <View style={styles.cardHeaderContent}>
              <Text style={styles.cardHeaderTitle}>{name}</Text>
              <Text style={styles.cardHeaderSubtitle}>{jabatan}</Text>
            </View>
            <Ionicons
              name={'md-settings-outline'}
              color={'#fff'}
              size={hp(4)}
              onPress={() => navigation.navigate('Setting')}
            />
          </View>
        </View>
        <View style={styles.content}>
          <CardBox
            label="Absen Masuk"
            iconName="clock"
            SecondIconName="ios-arrow-up-circle-outline"
            onPress={() =>
              dispatch(checkAttendance({type_absen: 1}, navigation))
            }
          />
          <CardBox
            label="Absen Pulang"
            iconName="clock"
            SecondIconName="ios-arrow-down-circle-outline"
            onPress={() =>
              dispatch(checkAttendance({type_absen: 4}, navigation))
            }
          />
          <CardBox
            label="Riwayat Absen"
            iconName="history"
            onPress={() => dispatch(getHistory(navigation))}
          />
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  cardHeader: {
    height: hp(18),
    backgroundColor: '#3742fa',
    justifyContent: 'flex-end',
    borderRadius: 12,
  },
  cardHeaderBody: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: wp(3),
    marginLeft: 8,
  },
  content: {
    flexDirection: 'row',
    // backgroundColor: '#f5f6fa',
    paddingHorizontal: wp(9),
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    paddingTop: hp(3),
    flexWrap: 'wrap',
  },
  cardHeaderContent: {marginBottom: hp(2), marginTop: hp(5)},
  cardHeaderTitle: {fontSize: hp(4), fontWeight: 'bold', color: '#fff'},
  cardHeaderSubtitle: {fontSize: hp(2.2), fontWeight: 'bold', color: '#fff'},
  contentContainer: {
    flex: 1,
    backgroundColor: 'red',
    backgroundColor: '#f5f6fa',
    paddingHorizontal: wp(3),
  },
  contentTitle: {
    fontWeight: 'bold',
    fontSize: hp(2),
    marginBottom: hp(2),
  },
});

export default HomeScreen;
