import {useFormik} from 'formik';
import React, {useState} from 'react';
import {Keyboard, StatusBar, StyleSheet, View} from 'react-native';
import {Snackbar} from 'react-native-paper';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
import Button from '../../../Component/Atoms/Button';
import InputText from '../../../Component/Atoms/InputText';
import allActions from '../../../Config/Redux/Actions';
import {updatePasswordValidationSchema} from '../../../Config/Validation/UpdatePassword';

const initialValue = {
  oldPassword: '',
  newPassword: '',
  confirmNewPassword: '',
};
const UpdatePassword = ({navigation}) => {
  const dispatch = useDispatch();
  const {setting, attendance} = useSelector(state => state);
  const {loading} = setting || {};
  const {visible, message} = attendance;
  const {updatePassword} = allActions.settingAction;
  const [form, setForm] = useState(initialValue);

  const formik = useFormik({
    initialValues: form,
    enableReinitialize: true,
    onSubmit: (e, a) => onFormSubmit(e, a),
    validationSchema: updatePasswordValidationSchema,
  });

  const {
    values,
    errors,
    setFieldTouched,
    touched,
    handleSubmit,
    setFieldValue,
  } = formik;

  const onChangeText = ({name, val}) => {
    setFieldTouched(name);
    setFieldValue(name, val);
    setForm(prevState => ({...prevState, [name]: val}));
  };

  const onFormSubmit = (
    {oldPassword, newPassword},
    {setSubmitting, resetForm},
  ) => {
    Keyboard.dismiss();
    const data = {oldPassword, newPassword};
    // resetForm(initialValue);
    setSubmitting(false);
    console.log(data);
    dispatch(updatePassword(data, navigation));
    setForm(initialValue);
  };

  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="#130f40" />
      <Snackbar visible={visible}>{message}</Snackbar>
      <View style={styles.headerSection} />
      <View style={styles.content}>
        <View style={{flex: 1, marginTop: hp(4), paddingHorizontal: wp(3)}}>
          <InputText
            placeholder="Kata Sandi Sebelumnya"
            name="oldPassword"
            value={values.oldPassword}
            error={errors.oldPassword}
            onChangeText={e => onChangeText(e)}
            onBlur={e => setFieldTouched(e)}
            isError={touched.oldPassword}
            secureTextEntry
          />
          <InputText
            placeholder="Kata Sandi Baru"
            name="newPassword"
            value={values.newPassword}
            error={errors.newPassword}
            onChangeText={e => onChangeText(e)}
            onBlur={e => setFieldTouched(e)}
            isError={touched.newPassword}
            secureTextEntry
          />
          <InputText
            placeholder="Konfirmasi Kata Sandi"
            name="confirmNewPassword"
            value={values.confirmNewPassword}
            error={errors.confirmNewPassword}
            onChangeText={e => onChangeText(e)}
            onBlur={e => setFieldTouched(e)}
            isError={touched.confirmNewPassword}
            secureTextEntry
          />
          <Button
            label={'UBAH KATA SANDI'}
            onPress={handleSubmit}
            loading={loading}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  headerSection: {
    backgroundColor: '#130f40',
    height: hp(15),
  },
  content: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopEndRadius: hp(3),
    borderTopStartRadius: hp(3),
    marginTop: -hp(3),
  },
  contentBody: {
    backgroundColor: '#fff',
    borderTopEndRadius: hp(3),
    borderTopStartRadius: hp(3),
    borderBottomColor: '#f1f2f6',
    borderBottomWidth: 1,
  },
  contentProfile: {alignItems: 'center', marginTop: -hp(6)},
  contentProfileName: {fontSize: hp(3)},
  contentProfileEmail: {
    fontSize: hp(2),
    color: '#636e72',
    paddingBottom: hp(2),
  },
  contentActivityInfo: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: hp(2),
  },
  contentStatus: {
    backgroundColor: '#fff',
    paddingTop: hp(1),
    paddingBottom: hp(0.5),
    paddingHorizontal: hp(3),
    marginBottom: hp(2),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  contentAccount: {
    backgroundColor: '#fff',
    paddingVertical: hp(1),
    paddingHorizontal: hp(3),
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: hp(2),
    borderBottomColor: '#f1f2f6',
    borderBottomWidth: 1,
  },
});

export default UpdatePassword;
