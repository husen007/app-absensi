import React from 'react';
import {StatusBar, StyleSheet, Text, View} from 'react-native';
import {Snackbar} from 'react-native-paper';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
import Button from '../../Component/Atoms/Button';
import ProfileImage from '../../Component/Atoms/ProfileImage';
import ProfileItem from '../../Component/Atoms/ProfileItem';
import allActions from '../../Config/Redux/Actions';
import DeviceInfo from 'react-native-device-info';
import {launchImageLibrary} from 'react-native-image-picker';

const options = {
  mediaType: 'photo',
  saveToPhotos: false,
};
const Setting = ({navigation}) => {
  const dispatch = useDispatch();
  const {auth, attendance, setting} = useSelector(state => state);
  const {visible, message} = attendance;
  const {photoLoading} = setting;

  const {name, jabatan, fotoKaryawan} = auth.user || {};
  const {logout, updateProfilePhoto} = allActions.settingAction;

  const onSelectImage = file => {
    if (file.assets && file.assets.length > 0) {
      const fileData = file.assets[0];
      const data = {
        name: fileData.fileName,
        type: fileData.type,
        uri: fileData.uri.replace('file:///', 'file:/'),
      };
      // console.log(data);
      let formData = new FormData();
      formData.append('foto_karyawan', data);
      dispatch(updateProfilePhoto(formData));
    }
  };

  console.log(fotoKaryawan || setting.userProfilePhoto);
  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="#130f40" />
      <Snackbar visible={visible}>{message}</Snackbar>
      <View style={styles.headerSection} />
      <View style={styles.content}>
        <View style={styles.contentBody}>
          <View style={styles.contentProfile}>
            <ProfileImage
              size={30}
              loading={photoLoading}
              source={{uri: fotoKaryawan || setting.userProfilePhoto}}
              onPress={() => launchImageLibrary(options, onSelectImage)}
            />
            <Text style={styles.contentProfileName}>{name}</Text>
            <Text style={styles.contentProfileEmail}>{jabatan}</Text>
          </View>
        </View>

        <View style={{flex: 1, marginTop: hp(2)}}>
          <ProfileItem
            label="Ubah Kata Sandi"
            iconName="key"
            onPress={() => navigation.navigate('UpdatePassword')}
          />
          {/* <ProfileItem label="Jadwal Absen" iconName="clock" /> */}
        </View>

        <View style={{paddingHorizontal: wp(3), paddingBottom: wp(5)}}>
          <Button
            label={'Logout'}
            buttonColor="#e74c3c"
            onPress={() => dispatch(logout(navigation))}
            loading={setting.loading}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  headerSection: {
    backgroundColor: '#130f40',
    height: hp(15),
  },
  content: {
    flex: 1,
    backgroundColor: '#f1f2f6',
    borderTopEndRadius: hp(3),
    borderTopStartRadius: hp(3),
    marginTop: -hp(3),
  },
  contentBody: {
    backgroundColor: '#fff',
    borderTopEndRadius: hp(3),
    borderTopStartRadius: hp(3),
    borderBottomColor: '#f1f2f6',
    borderBottomWidth: 1,
  },
  contentProfile: {alignItems: 'center', marginTop: -hp(6)},
  contentProfileName: {fontSize: hp(3)},
  contentProfileEmail: {
    fontSize: hp(2),
    color: '#636e72',
    paddingBottom: hp(2),
  },
  contentActivityInfo: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: hp(2),
  },
  contentStatus: {
    backgroundColor: '#fff',
    paddingTop: hp(1),
    paddingBottom: hp(0.5),
    paddingHorizontal: hp(3),
    marginBottom: hp(2),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  contentAccount: {
    backgroundColor: '#fff',
    paddingVertical: hp(1),
    paddingHorizontal: hp(3),
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: hp(2),
    borderBottomColor: '#f1f2f6',
    borderBottomWidth: 1,
  },
  textVersion: {
    textAlign: 'center',
    paddingBottom: hp(1),
    color: '#aeaeae',
  },
});

export default Setting;
