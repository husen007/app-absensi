import React from 'react';
import {FlatList, ScrollView, StyleSheet, Text, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
import HistoryCard from '../../Component/Molecules/HistoryCard';
import {mergeArray, orderData} from '../../Utils/Helper';
const HistoryScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {histories} = useSelector(state => state.attendance);
  if (histories && histories.length > 0) {
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <HistoryCard data={histories} />
        </View>
      </ScrollView>
    );
  }

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Tidak Ada Riwayat Absen</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, paddingHorizontal: wp(3), marginTop: hp(2)},
  card: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    borderRadius: hp(1.5),
    marginBottom: hp(2),
  },
  cardBody: {
    fontSize: hp(2),
    fontWeight: 'bold',
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 1,
    paddingBottom: hp(1),
  },
  cardItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: hp(1),
  },
});

export default HistoryScreen;
