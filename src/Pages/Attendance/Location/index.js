import Geolocation from '@react-native-community/geolocation';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {getDistance} from 'geolib';
import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableHighlight, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Snackbar} from 'react-native-paper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {useDispatch, useSelector} from 'react-redux';
import Button from '../../../Component/Atoms/Button';
import allActions from '../../../Config/Redux/Actions';

const LocationAtendance = ({navigation, route}) => {
  const {params} = route;
  const dispatch = useDispatch();
  const {selectCoordinate, getAdressOnDrag} = allActions.attendanceAction;
  const {attendanceForm} = useSelector(state => state.attendance);
  const [latitude, setLatitude] = useState(0);
  const [longitude, setLongitude] = useState(0);
  const [distance, setDistance] = useState(0);
  const [visible, setVisible] = useState(0);
  const [curentLocation, setCurrentLocation] = useState({});
  useEffect(() => {
    getUserCurrentLocation();
  }, [curentLocation]);

  const getUserCurrentLocation = () => {
    const options = {
      enableHighAccuracy: true,
      timeout: 5000,
    };
    Geolocation.getCurrentPosition(onSuccess, onError, options);
  };
  const onSuccess = position => {
    const curentLocation = {
      latitude: position.coords.latitude,
      longitude: position.coords.longitude,
    };

    dispatch(selectCoordinate(curentLocation));
    setCurrentLocation(curentLocation);

    let data = {
      geometry: {
        coordinates: [position.coords.longitude, position.coords.latitude],
      },
    };
    onPointDrag(data);
  };
  const onError = error => {
    console.log(error);
  };

  const onPointDrag = data => {
    const {latitude, longitude} = params.coordinate;
    let distance = getDistance(
      {latitude, longitude},
      {
        latitude: data.geometry.coordinates[1],
        longitude: data.geometry.coordinates[0],
      },
    );
    let countDistance = distance / 1000;
    setDistance(countDistance);

    if (countDistance > 0.05) {
      setVisible(!visible);
    }

    setTimeout(() => {
      setVisible(false);
    }, 3000);

    const coordinate = [
      data.geometry.coordinates[0],
      data.geometry.coordinates[1],
    ];
    const dataCoordinate = {
      latitude: data.geometry.coordinates[1],
      longitude: data.geometry.coordinates[0],
    };
    dispatch(selectCoordinate(dataCoordinate));
    dispatch(getAdressOnDrag(coordinate));
  };

  const handlePressConfirm = () => {
    navigation.replace('AttendanceRecognition', {
      type_absen: params.data.type_absen,
    });
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Snackbar visible={visible}>
        Lokasi kamu diluar radius. maks 50 m dari titik absen
      </Snackbar>
      <View
        style={{
          width: '100%',
          height: '100%',
          zIndex: -1,
        }}>
        <TouchableHighlight
          onPress={() => getUserCurrentLocation()}
          underlayColor="#fff"
          style={style.mapFocus}>
          <MaterialIcons name="filter-center-focus" size={hp(5)} />
        </TouchableHighlight>
        <View
          style={{
            flex: 1,
            width: '100%',
          }}>
          <MapboxGL.MapView
            style={{flex: 1, borderRadius: wp(5)}}
            zoomEnabled
            compassEnabled
            styleURL={MapboxGL.StyleURL.Street}
            localizeLabels>
            <MapboxGL.Camera
              zoomLevel={15}
              animationMode={'moveTo'}
              // animationDuration={1100}
              centerCoordinate={[
                params.coordinate.longitude,
                params.coordinate.latitude,
              ]}
              // followUserLocation={true}
              followUserMode={MapboxGL.UserTrackingModes.Follow}
              focusable
              // centerCoordinate={coordinates}
            />

            <MapboxGL.PointAnnotation
              key="key1"
              id="id1"
              title="Test"
              coordinate={[
                params.coordinate.longitude,
                params.coordinate.latitude,
              ]}>
              <Icon name={'home'} size={hp(3)} color="#4834d4" />
            </MapboxGL.PointAnnotation>

            <MapboxGL.PointAnnotation
              key="key2"
              id="id2"
              title="Lokasi absen"
              draggable
              style={{flex: 1}}
              // onDrag={e => onPointDrag(e)}
              onDragEnd={e => onPointDrag(e)}
              coordinate={[curentLocation.longitude, curentLocation.latitude]}>
              <Icon name={'map-pin'} size={hp(4)} color="#192a56" />
            </MapboxGL.PointAnnotation>
          </MapboxGL.MapView>
        </View>
      </View>

      <View
        style={{
          flex: 1,
          width: wp(100),
          paddingHorizontal: wp(2.6),
          position: 'absolute',
          bottom: hp(1),
          left: 0,
          right: 0,
        }}>
        <Button
          label="KONFIRMASI"
          onPress={handlePressConfirm}
          disabled={distance > 0.05}
        />
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  mapFocus: {
    position: 'absolute',
    top: hp(2),
    right: wp(3),
    zIndex: 9999,
    backgroundColor: '#fff',
    borderRadius: hp(1),
  },
});

export default LocationAtendance;
