import React, {Component} from 'react';
import {
  DeviceEventEmitter,
  Image,
  NativeModules,
  StatusBar,
  View,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import allActions from '../../../Config/Redux/Actions';
import {useRef, useEffect} from 'react';

const {RecognitionModule} = NativeModules;
const {updatePhoto, submitAttendance} = allActions.attendanceAction;

class AttendanceRecognition extends Component {

  constructor(props) {
    super(props);
    this.state = {
      status: false,
    };
    DeviceEventEmitter.addListener('onSuccess', this.onSuccess);
  }

  componentDidMount() {
    if (!this.state.status) {
      this.onShow();
    }
  }

  
  onShow = () => {
    return RecognitionModule.navigateToRecognition();
  };

  onSuccess = async e => {
    this.setState({status: e.status});
    const data = {
      foto: e.uri,
    };

    this.props.updatePhoto(data);

    if (e.status) {
      const data = {
        ...this.props.attendanceForm,
        type_absen: this.props.route.params.type_absen,
      };

      await this.props.submitAttendance(data, this.props.navigation);
      // return this.props.navigation.replace('HomeScreen');
    }
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fff',
        }}>
        <StatusBar backgroundColor="#fff" translucent />
        <Image
          source={require('../../../Assets/Image/loader.gif')}
          style={{width: wp(50), height: wp(50)}}
        />
      </View>
    );
  }
}

// export function useIsMounted() {
//   const isMounted = useRef(false);

//   useEffect(() => {
//     isMounted.current = true;
//     return () => (isMounted.current = false);
//   }, []);

//   return isMounted;
// }

const mapStateToprops = ({attendance}) => ({
  attendanceForm: attendance.attendanceForm,
});

export default connect(mapStateToprops, {updatePhoto, submitAttendance})(
  AttendanceRecognition,
);
