import React, {useState} from 'react';
import {
  KeyboardAvoidingView,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  ImageBackground,
} from 'react-native';
import Button from '../../Component/Atoms/Button';
import InputText from '../../Component/Atoms/InputText';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
import allActions from '../../Config/Redux/Actions';

const LoginScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const state = useSelector(state => state.auth);
  const {login} = allActions.authAction;
  const [form, setForm] = useState({username: '', password: ''});
  const onChangeText = ({name, val}) => {
    setForm(prevState => ({...prevState, [name]: val}));
  };

  return (
    <>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.container}>
        <ImageBackground
          source={require('../../Assets/Image/bg_biru.png')}
          style={{
            width: '100%', // applied to Image
            height: '100%',
          }}>
          <SafeAreaView style={styles.container}>
            <StatusBar backgroundColor={'#130f40'} />

            <View style={styles.contentContainer}>
              <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : undefined}
                keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>
                <View style={styles.cardContainer}>
                  <Text style={styles.cardTitle}>Login</Text>
                  <View style={{marginTop: hp(5)}}>
                    <InputText
                      placeholder="Username"
                      name="username"
                      onChangeText={e => onChangeText(e)}
                      value={form.username}
                      onBlur={e => console.log(e)}
                    />
                    <InputText
                      placeholder="Password"
                      name="password"
                      onChangeText={e => onChangeText(e)}
                      value={form.password}
                      secureTextEntry
                      onBlur={e => console.log(e)}
                    />

                    {/* tombol submit */}
                    <Button
                      loading={state.isLoading}
                      label="Login"
                      onPress={() => dispatch(login(form))}
                    />
                  </View>
                </View>
              </KeyboardAvoidingView>
            </View>
          </SafeAreaView>
        </ImageBackground>
      </KeyboardAvoidingView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1},
  contentContainer: {
    flex: 1,
    paddingHorizontal: wp(3),
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardContainer: {
    width: wp(90),
    backgroundColor: '#fff',
    borderRadius: wp(5),
    paddingHorizontal: wp(5),
    paddingVertical: hp(2),
  },
  cardTitle: {
    fontSize: wp(6),
    fontWeight: 'bold',
    marginTop: hp(1),
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardSubtitle: {fontSize: wp(4)},
});

export default LoginScreen;
