import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/Entypo';
import {useDispatch} from 'react-redux';
import LocationAtendance from '../../../Pages/Attendance/Location';
import AttendanceRecognition from '../../../Pages/Attendance/Recognition';
import HistoryScreen from '../../../Pages/HistoryScreen';
import HomeScreen from '../../../Pages/HomeScreen';
import LocationScreen from '../../../Pages/LocationScreen';
import TrainingScreen from '../../../Pages/TrainingScreen';
import UpdatePassword from '../../../Pages/Setting/UpdatePassword/index';
import Setting from '../../../Pages/Setting/index';
import allActions from '../../Redux/Actions';

const Stack = createStackNavigator();
const ScureStack = ({initialRouteName}) => {
  const dispatch = useDispatch();
  const {resetForm} = allActions.attendanceAction;
  if (initialRouteName === 'TrainingScreen') {
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="TrainingScreen"
          component={TrainingScreen}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    );
  }
  if (initialRouteName === 'LocationScreen') {
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="LocationScreen"
          component={LocationScreen}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    );
  }
  // if (!initialRouteName) {
  //   return (
  //     <Stack.Navigator>
  //       <Stack.Screen
  //         name="SplashScreen"
  //         component={Splash}
  //         options={{headerShown: false}}
  //       />
  //     </Stack.Navigator>
  //   );
  // }
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="AttendanceLocation"
        component={LocationAtendance}
        options={({navigation}) => ({
          title: 'Lokasi Absen',
          headerTitleAlign: 'center',
          headerLeftContainerStyle: {
            paddingLeft: hp(2),
          },
          headerLeft: () => (
            <Icon
              name="chevron-left"
              size={hp(3.5)}
              onPress={() => {
                navigation.pop();
                dispatch(resetForm());
              }}
            />
          ),
        })}
      />
      <Stack.Screen
        name="HistoryScreen"
        component={HistoryScreen}
        options={({navigation}) => ({
          title: 'Riwayat Absensi',
          headerTitleAlign: 'center',
          headerLeftContainerStyle: {
            paddingLeft: hp(2),
          },
          headerLeft: () => (
            <Icon
              name="chevron-left"
              size={hp(3.5)}
              onPress={() => {
                navigation.goBack();
                dispatch(resetForm());
              }}
            />
          ),
        })}
      />
      <Stack.Screen
        name="AttendanceRecognition"
        component={AttendanceRecognition}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Setting"
        component={Setting}
        options={({navigation, route}) => ({
          title: 'Profile',
          headerTitleStyle: {fontWeight: 'bold', color: '#fff'},
          headerTitleAlign: 'center',

          headerStyle: {
            backgroundColor: '#130f40',
            borderBottomWidth: 0,
            elevation: 0,
          },
          headerLeftContainerStyle: {
            paddingLeft: hp(2),
          },
          headerLeft: () => (
            <Icon
              name="chevron-left"
              size={hp(3.5)}
              color="#fff"
              onPress={() => {
                navigation.goBack();
              }}
            />
          ),
        })}
      />
      <Stack.Screen
        name="UpdatePassword"
        component={UpdatePassword}
        options={({navigation, route}) => ({
          title: 'Update Kata Sandi',
          headerTitleStyle: {fontWeight: 'bold', color: '#fff'},
          headerTitleAlign: 'center',

          headerStyle: {
            backgroundColor: '#130f40',
            borderBottomWidth: 0,
            elevation: 0,
          },
          headerLeftContainerStyle: {
            paddingLeft: hp(2),
          },
          headerLeft: () => (
            <Icon
              name="chevron-left"
              size={hp(3.5)}
              color="#fff"
              onPress={() => {
                navigation.goBack();
              }}
            />
          ),
        })}
      />
    </Stack.Navigator>
  );
};

export default ScureStack;
