import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import LoginScreen from '../../../Pages/Login';
import Splash from '../../../Pages/Splash';

const Stack = createStackNavigator();
const AuthStack = ({initialRouteName = 'SplashScreen'}) => {
  return (
    <Stack.Navigator initialRouteName={initialRouteName}>
      <Stack.Screen
        name="SplashScreen"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default AuthStack;
