import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useState} from 'react';
import SplashScreen from 'react-native-splash-screen';
import {useDispatch, useSelector} from 'react-redux';
import Splash from '../../Pages/Splash';
import {setHeaderToken} from '../Axios/setHeaderToken';
import allActions from '../Redux/Actions';
import AuthStack from './AuthStack';
import ScureStack from './ScureStack';

const Routes = () => {
  const state = useSelector(state => state);
  const dispatch = useDispatch();
  const {setUser} = allActions.authAction;
  const {auth, attendance} = state;
  const [loading, setLoading] = useState(attendance.loading);
  const [routeName, setRouteName] = useState('HomeScreen');
  const [token, setToken] = useState(auth.token);

  const getToken = async token => {
    if (!token) {
      const dataToken = await AsyncStorage.getItem('token');
      setToken(dataToken);
      setHeaderToken(dataToken);
    } else {
      setToken(token);
      setHeaderToken(token);
    }
  };
  const getUser = async user => {
    if (!user) {
      const dataUser = await AsyncStorage.getItem('user');
      dispatch(setUser(JSON.parse(dataUser)));
    } else {
      dispatch(setUser(user));
    }
  };

  const getRouteName = async routeName => {
    if (!routeName) {
      const dataRouteName = await AsyncStorage.getItem('initialRouteName');
      setRouteName(dataRouteName);
      // setLoading(false);
      SplashScreen.hide();
    } else {
      SplashScreen.hide();
      setRouteName(routeName);
    }
  };

  useEffect(() => {
    getToken(auth.token);
    getRouteName(auth.initialRouteName);
    if (!auth.user) {
      getUser(auth.user);
    }
  }, [auth]);

  if (token) {
    return <ScureStack initialRouteName={routeName} />;
  }

  return <AuthStack initialRouteName={'LoginScreen'} />;
};

export default Routes;
