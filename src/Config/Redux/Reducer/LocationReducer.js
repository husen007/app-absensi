import {SET_COORDINATE, SET_LOCATION_DATA} from '../../constant';

const initialState = {
  cordinate: {
    location_name: null,
    latitude: null,
    longitude: null,
  },
  locationData: [],
};

const LocationReducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case SET_LOCATION_DATA:
      return {
        ...state,
        locationData: payload,
      };
    case SET_COORDINATE:
      return {
        ...state,
        cordinate: {
          ...state.cordinate,
          payload,
        },
      };
    default:
      return {...state};
  }
};
export default LocationReducer;
