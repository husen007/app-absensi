import {
  LOGOUT,
  SET_INITIAL_ROUTE_NAME,
  SET_LOADING,
  SET_MESSAGE,
  SET_TOKEN,
  SET_USER,
} from '../../constant';

const initialState = {
  user: null,
  token: null,
  isLoading: false,
  initialRouteName: null,
  message: null,
};

const AuthReducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case SET_TOKEN:
      return {
        ...state,
        token: payload,
      };
    case SET_USER:
      return {
        ...state,
        user: payload,
      };
    case SET_MESSAGE:
      return {
        ...state,
        message: payload,
      };
    case LOGOUT:
      return {
        ...state,
        user: {},
        token: null,
        isLoading: false,
        initialRouteName: null,
        message: null,
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: !state.isLoading,
      };
    case SET_INITIAL_ROUTE_NAME:
      return {
        ...state,
        initialRouteName: payload,
      };

    default:
      return {...state};
  }
};
export default AuthReducer;
