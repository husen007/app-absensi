import {combineReducers} from 'redux';
import AuthReducer from './AuthReducer';
import AttendanceReducer from './AttendanceReducer';
import LocationReducer from './LocationReducer';
import SettingReducer from './SettingReducer';

const rootReducer = combineReducers({
  auth: AuthReducer,
  attendance: AttendanceReducer,
  location: LocationReducer,
  setting: SettingReducer,
});

export default rootReducer;
