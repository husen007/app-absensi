import {
  SET_PROFILE_PHOTO_LOADING,
  SET_USER_PHOTO,
  USER_LOADING,
} from '../../constant';

const initialState = {
  loading: false,
  photoLoading: false,
  userProfilePhoto:
    'https://cdn2.iconfinder.com/data/icons/avatars-99/62/avatar-370-456322-512.png',
};

const SettingReducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case USER_LOADING:
      return {
        ...state,
        loading: !state.loading,
      };
    case SET_PROFILE_PHOTO_LOADING:
      return {
        ...state,
        photoLoading: !state.photoLoading,
      };
    case SET_USER_PHOTO:
      return {
        ...state,
        userProfilePhoto: payload,
      };
    default:
      return {...state};
  }
};
export default SettingReducer;
