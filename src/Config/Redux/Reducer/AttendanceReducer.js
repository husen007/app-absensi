import {
  RESET_ATTENDANCE_FORM,
  SET_HISTORY,
  SET_LOADING,
  SET_MESSAGE,
  SET_VISIBLE,
  UPDATE_ATTENDANCE_FORM,
} from '../../constant';

const initialState = {
  loading: false,
  visible: false,
  message: null,
  histories: [],
  attendanceForm: {
    latitude: null,
    longitude: null,
    nama_lokasi: null,
    foto: null,
  },
};

const AttendanceReducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case SET_MESSAGE:
      return {
        ...state,
        message: payload,
      };
    case SET_LOADING:
      return {
        ...state,
        loading: !state.loading,
      };
    case SET_VISIBLE:
      return {
        ...state,
        visible: !state.visible,
      };
    case UPDATE_ATTENDANCE_FORM:
      return {
        ...state,
        attendanceForm: {...state.attendanceForm, ...payload},
      };
    case SET_HISTORY:
      return {
        ...state,
        histories: payload,
      };
    case RESET_ATTENDANCE_FORM:
      return {
        ...state,
        attendanceForm: {
          latitude: null,
          longitude: null,
          nama_lokasi: null,
          foto: null,
        },
      };
    default:
      return {...state};
  }
};
export default AttendanceReducer;
