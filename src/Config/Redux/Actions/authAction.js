import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {
  API_URL,
  LOGOUT,
  SET_INITIAL_ROUTE_NAME,
  SET_LOADING,
  SET_TOKEN,
  SET_USER,
} from '../../constant';
import SharedPreferences from 'react-native-shared-preferences';
import {ToastAndroid} from 'react-native';

SharedPreferences.setName('HashMap');
const setToken = token => {
  return {
    type: SET_TOKEN,
    payload: token,
  };
};

const setUser = user => {
  return {
    type: SET_USER,
    payload: user,
  };
};

const logout = () => {
  return {
    type: LOGOUT,
    payload: false,
  };
};

const setLoading = () => {
  return {type: SET_LOADING};
};

const setInitialRouteName = initialRouteName => {
  return {
    type: SET_INITIAL_ROUTE_NAME,
    payload: initialRouteName,
  };
};

const login = data => async dispatch => {
  dispatch(setLoading());
  await axios
    .post(`${API_URL}/auth/login`, data)
    .then(async result => {
      if (result.data) {
        let response = result.data;
        let {access_token, user} = response.data;
        let {latitude, longitude} = user.location;

        // dispatch user
        dispatch(setUser(user));
        dispatch(setToken(access_token));

        await AsyncStorage.setItem('user', JSON.stringify(user));
        await AsyncStorage.setItem('token', access_token);

        SharedPreferences.setItem('access_token', access_token);
        SharedPreferences.setItem('name', user.name);
        SharedPreferences.setItem('jabatan', user.jabatan);

        dispatch(setLoading());

        let face = user.haveFace ? true : false;
        let location = latitude && longitude ? true : false;

        if (face && location) {
          await AsyncStorage.setItem('initialRouteName', 'HomeScreen');
          return dispatch(setInitialRouteName('HomeScreen'));
        }

        if (!location && !face) {
          ToastAndroid.showWithGravity(
            'Silahkan training wajah anda terlebih dahulu',
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
          );
          await AsyncStorage.setItem('initialRouteName', 'TrainingScreen');
          return dispatch(setInitialRouteName('TrainingScreen'));
        }

        if (face && !location) {
          ToastAndroid.showWithGravity(
            'Silahkan tambahkan lokasi anda terlebih dahulu',
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
          );
          await AsyncStorage.setItem('initialRouteName', 'LocationScreen');
          return dispatch(setInitialRouteName('LocationScreen'));
        }

        if (location && !face) {
          ToastAndroid.showWithGravity(
            'Silahkan training wajah anda terlebih dahulu',
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
          );
          await AsyncStorage.setItem('initialRouteName', 'TrainingScreen');
          return dispatch(setInitialRouteName('TrainingScreen'));
        }

        await AsyncStorage.setItem('initialRouteName', 'HomeScreen');
        return dispatch(setInitialRouteName('HomeScreen'));
      }
    })
    .catch(err => {
      let result = err.response;
      if (result?.data) {
        ToastAndroid.showWithGravity(
          result.data.message,
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
        return dispatch(setLoading());
      }
      ToastAndroid.showWithGravity(
        'Terjadi Kesalahan!',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
      return dispatch(setLoading());
    });
};

export default {
  setInitialRouteName,
  setToken,
  setUser,
  logout,
  setLoading,
  login,
};
