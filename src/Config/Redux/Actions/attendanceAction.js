import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import allActions from '.';
import {
  API_URL,
  MAPBOX_KEY,
  RESET_ATTENDANCE_FORM,
  SET_HISTORY,
  SET_LOADING,
  SET_MESSAGE,
  SET_VISIBLE,
  UPDATE_ATTENDANCE_FORM,
} from '../../constant';

const setLoading = () => {
  return {
    type: SET_LOADING,
  };
};

const setVisible = () => {
  return {
    type: SET_VISIBLE,
  };
};

const setMessage = message => {
  return {
    type: SET_MESSAGE,
    payload: message,
  };
};

const checkAttendance = (data, navigation) => async dispatch => {
  dispatch(setLoading());
  await axios
    .post(`${API_URL}/cek-absen`, data)
    .then(result => {
      const {message, next, coordinate, alert} = result.data;
      if (next) {
        dispatch(setLoading());
        return navigation.push('AttendanceLocation', {
          coordinate: result.data.coordinate,
          data,
        });
      }
      dispatch(setLoading());
      dispatch(setMessage(message));
      dispatch(setVisible());

      setTimeout(() => {
        dispatch(setMessage(null));
        dispatch(setVisible());
      }, 2000);
    })
    .catch(err => console.log(err));
};

const submitAttendance = (data, navigation) => async dispatch => {
  const {setInitialRouteName} = allActions.authAction;
  dispatch(setLoading());
  await axios
    .post(`${API_URL}/absen`, data)
    .then(async result => {
      const {message} = result.data;
      await AsyncStorage.setItem('initialRouteName', 'HomeScreen');
      dispatch(setLoading());
      dispatch(resetForm());
      dispatch(setInitialRouteName('HomeScreen'));
      dispatch(setMessage(message));
      dispatch(setVisible());
      dispatch(getHistory(null, false));

      setTimeout(() => {
        dispatch(setMessage(null));
        dispatch(setVisible());
      }, 2000);
      return navigation.replace('HomeScreen');
    })
    .catch(err => {
      dispatch(setLoading());
      console.log(err);
      return navigation.replace('HomeScreen');
    });
};

const getAdressOnDrag = coordinate => dispatch => {
  axios
    .get(
      `https://api.mapbox.com/geocoding/v5/mapbox.places/${coordinate}.json?types=poi&access_token=${MAPBOX_KEY}`,
    )
    .then(results => {
      if (results.data.features.length > 0) {
        return dispatch(
          selectCoordinate({nama_lokasi: results.data.features[0].place_name}),
        );
      }
      dispatch(selectCoordinate({nama_lokasi: null}));
    });
};

const getHistory = (navigation, callType = true) => async dispatch => {
  await axios
    .get(`${API_URL}/riwayat-absen`)
    .then(results => {
      dispatch(setHistory(results.data.data));

      callType && navigation.navigate('HistoryScreen');
    })
    .catch(e => {
      dispatch(setHistory([]));
    });
};

const selectCoordinate = data => {
  return {type: UPDATE_ATTENDANCE_FORM, payload: data};
};
const setHistory = data => {
  return {type: SET_HISTORY, payload: data};
};

const updatePhoto = data => dispatch => {
  dispatch({type: UPDATE_ATTENDANCE_FORM, payload: data});
};

const resetForm = () => {
  return {type: RESET_ATTENDANCE_FORM};
};

export default {
  setMessage,
  setLoading,
  setVisible,
  checkAttendance,
  selectCoordinate,
  updatePhoto,
  resetForm,
  submitAttendance,
  getAdressOnDrag,
  getHistory,
};
