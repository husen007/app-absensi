import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {ToastAndroid} from 'react-native';
import allActions from '.';
import {
  API_URL,
  SET_PROFILE_PHOTO_LOADING,
  SET_USER_PHOTO,
  USER_LOADING,
} from '../../constant';

const updatePassword = (data, navigation) => async dispatch => {
  const {setMessage, setVisible} = allActions.attendanceAction;
  console.log('updatePassword', data);
  dispatch(setLoading());
  await axios
    .post(`${API_URL}/update-password`, data)
    .then(result => {
      const {message} = result.data;

      navigation.goBack();
      dispatch(setLoading());
      dispatch(setMessage(message));
      dispatch(setVisible());

      setTimeout(() => {
        dispatch(setMessage(null));
        dispatch(setVisible());
      }, 2000);
    })
    .catch(err => {
      const result = err.response;
      console.log(result);
      if (result) {
        let {message} = result.data;
        dispatch(setLoading());
        dispatch(setMessage(message));
        dispatch(setVisible());

        setTimeout(() => {
          dispatch(setMessage(null));
          dispatch(setVisible());
        }, 2000);
      }
    });
};

const updateProfilePhoto = data => async dispatch => {
  const {setMessage, setVisible} = allActions.attendanceAction;
  dispatch(setPhotoLoading());
  await axios
    .post(`${API_URL}/update-profile`, data, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    })
    .then(result => {
      const {message, data} = result.data;
      dispatch(setUserPhoto(data.foto_karyawan));

      dispatch(setMessage(message));
      dispatch(setVisible());

      setTimeout(() => {
        dispatch(setMessage(null));
        dispatch(setVisible());
      }, 2000);
    })
    .catch(err => {
      dispatch(setPhotoLoading());
    });
};

const logout = navigation => async dispatch => {
  const {setUser, setToken} = allActions.authAction;
  dispatch(setLoading());
  await axios.post(`${API_URL}/logout`, {}).then(async result => {
    const {message} = result.data;
    dispatch(setLoading());

    dispatch(setUser(null));
    dispatch(setToken(null));

    await AsyncStorage.removeItem('user');
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('isLogin');

    dispatch(setUser(null));
    dispatch(setToken(null));

    ToastAndroid.showWithGravity(
      message,
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );

    navigation.replace('LoginScreen');
  });
};

const setLoading = () => {
  return {type: USER_LOADING};
};

const setPhotoLoading = () => {
  return {type: SET_PROFILE_PHOTO_LOADING};
};

const setUserPhoto = path => async dispatch => {
  const {setUser} = allActions.authAction;

  const dataUser = await AsyncStorage.getItem('user');
  const user = JSON.parse(dataUser);
  const newUser = {...user, fotoKaryawan: path};
  await AsyncStorage.setItem('user', JSON.stringify(newUser));
  dispatch(setUser(newUser));
  dispatch(setTempUserPhoto(path));
  dispatch(setPhotoLoading());
};

const setTempUserPhoto = path => {
  return {type: SET_USER_PHOTO, payload: path};
};

export default {
  updatePassword,
  updateProfilePhoto,
  logout,
  setPhotoLoading,
};
