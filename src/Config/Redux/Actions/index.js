import authAction from './authAction';
import attendanceAction from './attendanceAction';
import locationAction from './locationAction';
import settingAction from './settingAction';

const allActions = {
  authAction,
  attendanceAction,
  locationAction,
  settingAction,
};

export default allActions;
