import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {ToastAndroid} from 'react-native';
import allActions from '.';
import {
  API_URL,
  MAPBOX_KEY,
  SET_COORDINATE,
  SET_LOADING,
  SET_LOCATION_DATA,
} from '../../constant';

const setCoordinate = location => {
  return {
    type: SET_COORDINATE,
    payload: location,
  };
};

const setLocationData = location => {
  return {
    type: SET_LOCATION_DATA,
    payload: location,
  };
};

const setLoading = () => {
  return {
    type: SET_LOADING,
  };
};

const addPointLocation = (data, navigation = null) => async dispatch => {
  const {setInitialRouteName} = allActions.authAction;
  dispatch(setLoading());
  await axios
    .post(`${API_URL}/input-lokasi`, data)
    .then(async result => {
      const {data} = result;

      await AsyncStorage.setItem('initialRouteName', 'HomeScreen');

      dispatch(setCoordinate(data));
      dispatch(setLoading());

      ToastAndroid.showWithGravity(
        'Titik lokasi berhasil ditambah',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );

      dispatch(setInitialRouteName('HomeScreen'));
    })
    .catch(err => {
      dispatch(setLoading());
    });
};

const searchPlace = (query, navigation = null) => async dispatch => {
  await axios
    .get(
      `https://api.mapbox.com/geocoding/v5/mapbox.places/${query}.json?access_token=${MAPBOX_KEY}`,
    )
    .then(results => {
      let data = [];

      if (results.data.features.length) {
        results.data.features.map(result => {
          data.push({
            text: result.text,
            place_name: result.place_name,
            latitude: result.center[1],
            longitude: result.center[0],
          });
        });
      }
      dispatch(setLocationData(data));
    })
    .catch(err => {
      console.log(err);
    });
};

const clearSearchPlace = () => dispatch => {
  dispatch(setLocationData([]));
};

export default {
  clearSearchPlace,
  searchPlace,
  setCoordinate,
  addPointLocation,
  setLoading,
  setLocationData,
};
