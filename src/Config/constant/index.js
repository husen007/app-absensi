export const SET_USER = 'SET_USER';
export const SET_TOKEN = 'SET_TOKEN';
export const SET_INITIAL_ROUTE_NAME = 'SET_INITIAL_ROUTE_NAME';
export const LOGOUT = 'LOGOUT';

export const SET_LOADING = 'SET_LOADING';

export const SET_VISIBLE = 'SET_VISIBLE';
export const SET_MESSAGE = 'SET_MESSAGE';

export const SET_LOCATION_DATA = 'SET_LOCATION_DATA';
export const SET_COORDINATE = 'SET_COORDINATE';

export const UPDATE_ATTENDANCE_FORM = 'UPDATE_ATTENDANCE_FORM';
export const RESET_ATTENDANCE_FORM = 'RESET_ATTENDANCE_FORM';
export const SET_HISTORY = 'SET_HISTORY';

export const USER_LOADING = 'USER_LOADING';
export const SET_PROFILE_PHOTO_LOADING = 'SET_PROFILE_PHOTO_LOADING';
export const SET_USER_PHOTO = 'SET_USER_PHOTO';

// API
export const API_URL = 'https://absensi.pt-mpi.com/api';
export const MAPBOX_KEY =
  'pk.eyJ1IjoieXVkaWNhbmRyYTEiLCJhIjoiY2tuemd6dXhoMDR1ZDJ2cGMzbGk0dHpoaSJ9.Y5TZzkmHQd4Q2hWpDllpsQ';
