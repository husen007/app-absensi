import * as yup from 'yup';

export const updatePasswordValidationSchema = yup.object().shape({
  oldPassword: yup
    .string()
    .required('Kata Sandi Sebelumnya Tidak Boleh Kosong'),
  newPassword: yup
    .string()
    .min(6, 'Kata sandi minimal terdiri 6 karakter')
    .required('Kata sandi tidak boleh kosong'),
  confirmNewPassword: yup
    .string()
    .oneOf([yup.ref('newPassword'), null], 'Kata sandi tidak sesuai')
    .required('Konfirmasi Kata sandi tidak boleh kosong'),
});
