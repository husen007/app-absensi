import React from 'react';
import {StyleSheet, Text, TouchableHighlight, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';

export const CardBox = ({
  onPress,
  label = 'label',
  PrimaryIcon = Icon,
  SecondIcon = Ionicons,
  iconName,
  SecondIconName,
}) => {
  return (
    <TouchableHighlight onPress={onPress} underlayColor={'transparent'}>
      <View style={[styles.card, styles.shadow]}>
        <IconTwo
          PrimaryIcon={PrimaryIcon}
          SecondIcon={SecondIcon}
          iconName={iconName}
          SecondIconName={SecondIconName}
        />
        <Text style={styles.cardTitle}>{label}</Text>
      </View>
    </TouchableHighlight>
  );
};

const IconTwo = ({
  PrimaryIcon,
  SecondIcon,
  iconName = null,
  SecondIconName = null,
}) => {
  if (!SecondIconName) {
    return <PrimaryIcon name={iconName} size={hp(5.6)} color="#0984e3" />;
  }
  return (
    <View>
      <PrimaryIcon name={iconName} size={hp(6)} color="#0984e3" />
      <View style={styles.iconContainer}>
        <SecondIcon
          name={SecondIconName}
          size={hp(2.5)}
          style={styles.childIcon}
          color="#0984e3"
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    width: wp(38),
    height: wp(38),
    marginBottom: hp(3),
    borderRadius: wp(5),
    justifyContent: 'center',
    alignItems: 'center',
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 4,
  },
  iconContainer: {
    width: hp(2.5),
    height: hp(2.5),
    backgroundColor: '#fff',
    borderRadius: hp(1.25),
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  childIcon: {
    position: 'absolute',
    bottom: 0.5,
    right: 0,
    left: 0.5,
    top: -0.5,
  },
  cardTitle: {fontSize: hp(2.2), paddingTop: hp(2.2)},
});
