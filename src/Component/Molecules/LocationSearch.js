import React, {useRef} from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import InputText from '../Atoms/InputText';

const LocationSearch = ({onChangeText, value, data = [], onPress, label}) => {
  const ref = useRef();

  return (
    <View style={{flex: 1}}>
      <TouchableHighlight
        underlayColor="#fff">
        <View style={styles.locationText}>
          <Text style={{paddingLeft: wp(3), color: '#7f8c8d'}}>
            {label ? label : 'Nama tempat akan tampil disini'}
          </Text>
        </View>
      </TouchableHighlight>
      <RBSheet
        ref={ref}
        height={hp(45)}
        openDuration={250}
        customStyles={style}>
        <>
          <View style={styles.listData}>
            <KeyboardAvoidingView
              behavior={Platform.OS === 'ios' ? 'padding' : undefined}
              keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>
              <View style={{flex: 1, width: wp(94)}}>
                <InputText
                  borderColor={'#ecf0f1'}
                  backgroundColor="#ecf0f1"
                  color="#7f8c8d"
                  placeholder={'Masukkan nama tempat atau alamat'}
                  onChangeText={onChangeText}
                  value={value}
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                  <View style={styles.itemList}>
                    {data &&
                      data.map((item, index) => (
                        <PlaceItem
                          key={index}
                          item={item}
                          onPress={() => onPress({item, ref: ref.current})}
                        />
                      ))}
                  </View>
                </ScrollView>
              </View>
            </KeyboardAvoidingView>
          </View>
        </>
      </RBSheet>
    </View>
  );
};

const PlaceItem = ({item, onPress}) => {
  return (
    <TouchableHighlight onPress={item => onPress(item)} underlayColor={'#fff'}>
      <View style={styles.item}>
        <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>{item.text}</Text>
        <Text style={{fontSize: hp(1.8)}}>{item.place_name}</Text>
      </View>
    </TouchableHighlight>
  );
};

const style = {
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
};

const styles = StyleSheet.create({
  locationText: {
    paddingVertical: hp(1.5),
    backgroundColor: '#ecf0f1',
    width: wp(94),
    justifyContent: 'center',
    marginBottom: hp(2),
    marginHorizontal: wp(3),
    borderRadius: wp(2),
  },
  listData: {
    flex: 1,
    paddingHorizontal: wp(3),
    paddingVertical: hp(2),
  },
  itemList: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    borderRadius: wp(2),
    borderColor: '#eaeaea',
    borderWidth: 1,
  },
  item: {
    backgroundColor: '#fff',
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 1,
    paddingVertical: hp(0.5),
  },
});

export default LocationSearch;
