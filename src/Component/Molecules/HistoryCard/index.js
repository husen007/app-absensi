import React from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {mergeArray, orderData} from '../../../Utils/Helper';
const HistoryCard = ({data}) => {
  return (
    <FlatList
      data={orderData(mergeArray(data))}
      renderItem={({item}) => (
        <View key={item.tanggal_absen} style={styles.card}>
          <View
            style={[
              styles.cardBody,
              {
                flexDirection: 'row',
                justifyContent: 'space-between',
              },
            ]}>
            <Text style={{fontWeight: 'bold', fontSize: hp(2)}}>
              {item.tanggal_absen}
            </Text>
            <Text
              style={{
                color: item.status_absen === 'TELAT' ? '#c0392b' : '#27ae60',
                fontWeight: 'bold',
              }}>
              {item.status_absen}
            </Text>
          </View>
          <FlatList
            data={orderData(item.items, 'asc')}
            renderItem={({item}) => <Item key={item.id} item={item} />}
          />
        </View>
      )}
    />
  );
};

const Item = ({item}) => {
  return (
    <View key={item.id} style={styles.cardItem}>
      <Text style={{fontSize: hp(2)}}>{item.type_absen}</Text>
      <Text
        style={{
          fontSize: hp(2),
          color: item.status_absen === 'TELAT' ? '#c0392b' : '#000',
        }}>
        {item.status_absen === 'BELUM ABSEN PULANG' ? '-' : item.jam_absen}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, paddingHorizontal: wp(3), marginTop: hp(2)},
  card: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    paddingVertical: hp(2),
    borderRadius: hp(1.5),
    marginBottom: hp(2),
  },
  cardBody: {
    fontSize: hp(2),
    fontWeight: 'bold',
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 1,
    paddingBottom: hp(1),
  },
  cardItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: hp(1),
  },
});

export default HistoryCard;
